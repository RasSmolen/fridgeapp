//
//  ViewController.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 25/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit

class InputController: UIViewController {

    @IBOutlet var ingredientsField: UITextField!
    
    @IBOutlet var addMoreIngredientsButton: UIButton!
    
    @IBOutlet var searchButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addMoreIngredientsButton.layer.cornerRadius = 5.0
        
    }


}

