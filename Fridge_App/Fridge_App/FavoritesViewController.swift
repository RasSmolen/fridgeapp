//
//  FavoritesViewController.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 05/12/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit
import CoreData
class CustomTableView: UITableViewCell, UITextFieldDelegate
{
    @IBOutlet var imageViewRecipe: UIImageView!
    @IBOutlet var textLabelRecipe: UILabel!
    
    
    
}
class FavoritesViewController: UIViewController {
   
    @IBOutlet var tableView: UITableView!
    var recipes : [Recipes] = []
    var requestHandler = RequestHandler()
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
                  self.fetchData()
                  self.tableView.reloadData()
                  
              }
              
              tableView.tableFooterView = UIView(frame: CGRect.zero)

              
              // Do any additional setup after loading the view.
          }
    
}
extension FavoritesViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return recipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "recipeToSave", for: indexPath) as? CustomTableView else{fatalError()}
        let newRecipe = recipes[indexPath.row]
        
        requestHandler.image(at: newRecipe.saveImage!) { imageResult in
           
           switch imageResult
           {
           case .success(let image):
               DispatchQueue.main.async {
                cell.imageViewRecipe.image = image
                   cell.setNeedsDisplay()
               }
           case.failure(let error):
               print(error)
           }
           
       }
        
        cell.textLabelRecipe.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.textLabelRecipe.text = newRecipe.savedRecipes
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let context = (CoreDataController.shared.mainContext)
        if  editingStyle == .delete
        {
            let recipe = recipes[indexPath.row]
            context.delete(recipe)
            recipes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
           
        }
        
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: recipes[indexPath.row].savedUrl!) else { return }
        UIApplication.shared.open(url)    }
    func fetchData()
    {
        let context = (CoreDataController.shared.mainContext)
        
        do
        {
            recipes = try context.fetch(Recipes.fetchRequest())
        }
        catch
        {
            print(error)
        }
    }
    
    
}
