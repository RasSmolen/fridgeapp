//
//  RequestHandler.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 03/12/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import UIKit.UIImage

enum APIError: Error {
    case incorrectStatusCode
    case invalidURL
    case dataIsNotImage
}

class RequestHandler
{
    
    let imageCache: NSCache<NSString, UIImage>
       
       init() { imageCache = NSCache<NSString, UIImage>() }
    
    private func data(at url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                completion(.failure(error))
            } else if let data = data, let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200...299:
                    completion(.success(data))
                default:
                    completion(.failure(APIError.incorrectStatusCode))
                }
                
            }
        }
        
        dataTask.resume()
        
    }
    
    func image(at urlString: String, completion: @escaping (Result<UIImage, Error>) -> Void) {
        
        let imageName = urlString as NSString
        
        if let image = imageCache.object(forKey: imageName) {
            completion(.success(image))
        } else {
            
            guard let imageURL = URL(string: urlString) else {
                completion(.failure(APIError.invalidURL))
                return
            }
            
            data(at: imageURL) { result in
                
                switch result {
                case .success(let data):
                    guard let image = UIImage(data: data) else {
                        completion(.failure(APIError.dataIsNotImage))
                        return
                    }
                    
                    self.imageCache.setObject(image, forKey: imageName)
                    completion(.success(image))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
                
            }
            
        }
        
    }

    func fetch(addtionalItems: [String], completion: @escaping (Result<Data, Error>) -> Void) {
        
        let appID = URLQueryItem(name: "app_id", value: "34679321")
        let appKey = URLQueryItem(name: "app_key", value: "998fc48e652ad63063bb57f28df55b55")
        let searchAmountFrom = URLQueryItem(name: "from", value: "0")
        let searchAmountTo = URLQueryItem(name: "to", value: "100")
        let query = URLQueryItem(name: "q" , value: "\([addtionalItems])" )

        
//"https://api.edamam.com/search?q=chicken&app_id=34679321&app_key=998fc48e652ad63063bb57f28df55b55&from=0&to=3&calories=591-722&health=alcohol-free"
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = "api.edamam.com"
            urlComponents.path = "/search"
        
        
        
        var queryItems = [appID, appKey,searchAmountFrom,searchAmountTo,query]
//
//        if let addtionalItems = addtionalItems {
//            queryItems.append(contentsOf: addtionalItems.map({ URLQueryItem(name: "", value: $0) }))
//        }
//
          urlComponents.queryItems = queryItems
        print(urlComponents)
     
        guard let url = urlComponents.url else {
            completion(.failure(APIError.invalidURL))
            return
        }
                
            data(at: url, completion: completion)
            
        }
}

    protocol Parser {
        func parse<T>(_ data: Data, into: T.Type, completion: (Result<T, Error>) -> Void) where T: Codable
    }

    struct JSONParser: Parser {
        
        func parse<T>(_ data: Data, into: T.Type, completion: (Result<T, Error>) -> Void) where T : Decodable, T : Encodable {
            
            do {
                let result = try JSONDecoder().decode(into, from: data)
                completion(.success(result))
                
            } catch {
              debugPrint(error)
            }
            
        }
        
}
