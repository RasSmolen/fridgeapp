
//  RecipeViewController.swift
//  Fridge_App
//  Created by Rastislav Smolen on 25/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.

import UIKit
import CoreData

struct CustomData {
    var title : String
    var image : String
    var url : String
    var ingredients : [String]
    var totalTime : Int
}
class CustomHeaderCell : UICollectionReusableView
{
    fileprivate let headerLabel  : UILabel =
    {
        let header = UILabel()
        header.translatesAutoresizingMaskIntoConstraints = false
        header.textColor = .black
        header.textAlignment = .center
        header.text = "Results"
        header.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        return header
        
    }()
    override init (frame: CGRect)
    {
        super.init(frame:frame)
        self.addSubview(headerLabel)
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo:self.leadingAnchor).isActive = true
        headerLabel.trailingAnchor.constraint(equalTo:self.trailingAnchor).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo:self.bottomAnchor).isActive = true
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct Images
{
    var image: UIImage
}

class CustomCell : UICollectionViewCell
{
    var data : Images?
    {
        didSet
        {
            guard let data = data else {return}
            imageView.image = data.image
            
        }
    }
    
    fileprivate let recipeNameLabel : UILabel =
    {
        let descriptionLabel = UILabel()
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = .black
        descriptionLabel.text = "Placeholder"
        return descriptionLabel
    }()
    
    fileprivate let imageView : UIImageView =
    {
        let imageOfRecipe = UIImageView()
        imageOfRecipe.translatesAutoresizingMaskIntoConstraints = false
        imageOfRecipe.contentMode = .scaleAspectFill
        imageOfRecipe.clipsToBounds = true
        imageOfRecipe.layer.cornerRadius = 5
        return imageOfRecipe
    }()
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        contentView.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        contentView.addSubview(recipeNameLabel)
        recipeNameLabel.backgroundColor = UIColor(white: 1, alpha: 0.5)
        recipeNameLabel.leadingAnchor.constraint(equalTo: imageView.leadingAnchor).isActive = true
        recipeNameLabel.trailingAnchor.constraint(equalTo: imageView.trailingAnchor).isActive = true
        recipeNameLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor,constant:-10).isActive = true
        recipeNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override var isSelected: Bool
        {
        didSet
        {
            imageView.layer.borderWidth = isSelected ? 100: 0
            imageView.layer.borderColor = UIColor.orange.withAlphaComponent(0.2).cgColor
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.borderWidth = 1
        isSelected = false
    }
    
}



class RecipeViewController: UIViewController
{
    var itemSelected: Int!
    var recipeOutput = [RecipesOutput]()
    
    let coreDataController = CoreDataController.shared
    
    let requestHandler = RequestHandler()
    var recipeJsonModel = RecipesOutput()
    var searchInput : [String] = []
    var listOfRecipes = [CustomData]()
    var imageArray: [String] = []
    
   

    func  goToDetailView()
    {
        performSegue(withIdentifier: UIStoryboardSegue.name.toDetailViewOfRecipe ,sender :nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let destination = segue.destination as? DetailRecipeViewController,
            segue.identifier == UIStoryboardSegue.name.toDetailViewOfRecipe
        {
            // cell = destination.data
            let recipe = listOfRecipes[itemSelected]
            destination.recipe = recipe
            destination.ingredients = listOfRecipes[itemSelected].ingredients
            destination.navBarTitle = listOfRecipes[itemSelected].title
          //  destination.recipeImages =
            destination.preparationTime = "\(listOfRecipes[itemSelected].totalTime)"
            destination.recipeUrl = listOfRecipes[itemSelected].url
        }
    }
    
    
    fileprivate let headerLabel  : UILabel =
    {
        let typeOfRecipeLabel = UILabel()
        typeOfRecipeLabel.translatesAutoresizingMaskIntoConstraints = false
        typeOfRecipeLabel.textAlignment = .center
        typeOfRecipeLabel.textColor = .black
        typeOfRecipeLabel.text = "Placeholder"
        return typeOfRecipeLabel
        
    }()
    fileprivate let collectionView: UICollectionView =
    {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionViewSetup = UICollectionView(frame: .zero,collectionViewLayout: layout)
        collectionViewSetup.translatesAutoresizingMaskIntoConstraints = false
        collectionViewSetup.register(CustomCell.self, forCellWithReuseIdentifier: "CollectionCell")
        collectionViewSetup.register(CustomHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCell")
        
        return collectionViewSetup
    }()
    
    var customData : CustomData?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.addSubview(collectionView)
        collectionView.backgroundColor = UIColor.clear
        
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        fetchData()
       
        recipeOutput = []
        listOfRecipes = []
    }
    func fetchData() {
        
        // Use the requestHandler to fetch the Users
        requestHandler.fetch(addtionalItems: searchInput) { (jsonResult) in

            switch jsonResult {
                
            case .success(let jsonData):
                
                      JSONParser().parse(jsonData, into: Root.self, completion: { [unowned self] outcome in
                          
                          switch outcome {
                          case .failure(let error): print(error)
                          case .success(let data): self.recipeJsonModel.recipe = data
                          }
                      })
                      
                
                      var ingredientsString : [String] = []
                      for elemelent in (self.recipeJsonModel.recipe?.hits.enumerated())!
                      {
                          ingredientsString.append(contentsOf: elemelent.element.recipe.ingredientLines)
                          self.listOfRecipes.append(CustomData(title: elemelent.element.recipe.label, image: elemelent.element.recipe.image,url: elemelent.element.recipe.url, ingredients: ingredientsString, totalTime: elemelent.element.recipe.totalTime))
                      }
                      
                      DispatchQueue.main.async {
                          self.collectionView.reloadData()
                      }
                
            case .failure(let error):
                print(error)
            }
            

            
        }
        //
    }
    
    
    
    
}
extension RecipeViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width :180, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfRecipes.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if(section==0) {
            return CGSize(width:collectionView.frame.size.width, height:50)
        } else if (section==1) {
            return CGSize(width:collectionView.frame.size.width, height:50)
        } else {
            return CGSize(width:collectionView.frame.size.width, height:50)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        guard let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCell", for: indexPath as IndexPath) as? CustomHeaderCell else
        {
            fatalError()
        }
        
        
        return headerCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CustomCell
        
        let recipe = listOfRecipes[indexPath.row]
       
            cell.imageView.image = #imageLiteral(resourceName: "Croisant")
            cell.recipeNameLabel.text = self.listOfRecipes[indexPath.row].title
        
        requestHandler.image(at: recipe.image) { imageResult in
            
            switch imageResult
            {
            case .success(let image):
                DispatchQueue.main.async {
                    cell.imageView.image = image
                    cell.setNeedsDisplay()
                }
            case.failure(let error):
                print(error)
            }
            
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        return false
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.isSelected = true
            
            itemSelected = indexPath.row
            
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.isSelected = false
            goToDetailView()
        }
    }
    
    
}
extension UIStoryboardSegue {
    
    enum name {
        static let toDetailViewOfRecipe = "toRecipe"
        static let toRestultsOfSearch = "toResults"
    }
    
}
