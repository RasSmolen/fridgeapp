//
//  DetailRecipeViewController.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 02/12/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit
import CoreData

class DetailRecipeViewController: UIViewController
{
    var recipes : [NSManagedObject] = []
    
    @IBOutlet var buttonToRecipeUrl: UIButton!
    
    @IBOutlet var preparationTimeLabel: UILabel!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var recipeImage: UIImageView!
    var coreData : CoreDataController!
    var recipe: CustomData!
    var data = RecipeViewController()
    let requestHandler = RequestHandler()
    
    var recipeImages : UIImage!
    var navBarTitle = ""
    var ingredients : [String] = []
    var preparationTime = ""
    var recipeUrl = ""
    var savedRecipes : [String] = []
    var recipesData : [String] = []
    var context = (CoreDataController.shared.mainContext)
    override func viewDidLoad()
    {
        super.viewDidLoad()
        navigationItem.title = navBarTitle
        preparationTimeLabel.text = "Preparation Time: \(preparationTime) min"
        
        requestHandler.image(at: recipe.image) { imageResult in
            
            switch imageResult
            {
            case .success(let image):
                DispatchQueue.main.async {
                    self.recipeImage.image = image
                }
            case.failure(let error):
                print(error)
            }
            
        }
        
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        

        
    }
    
    @IBAction func buttonToRecipe(_ sender: Any)
    {
        guard let url = URL(string: recipe.url) else { return }
        UIApplication.shared.open(url)
        
    }
    
    
    @IBAction func saveRecipeButton(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Save Recipe",
                                      message: "Add a favorite recipe",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default)
        {
            [unowned self] action in
            
            
            
            let newRecipe = NSEntityDescription.insertNewObject(forEntityName: "Recipes", into: self.context)
            newRecipe.setValue(self.navBarTitle, forKey: "savedRecipes")
            newRecipe.setValue(self.recipe.url, forKey: "savedUrl")
            newRecipe.setValue(self.recipe.image, forKey: "saveImage")
            
            do
            {
                try self.context.save()
            }catch
            {
                print(error)
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
        
        
        
        
        
        
    }
    
}
extension DetailRecipeViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientCell", for: indexPath)
        cell.textLabel?.text = self.ingredients[indexPath.row]
        
        return cell
        
    }
    
    
}
