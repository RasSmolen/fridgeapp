
//  InputViewController.swift
//  Fridge_App
//  Created by Rastislav Smolen on 25/11/2019.
//  Copyright © 2019 Ras-The Great-Smolen. All rights reserved.

import UIKit
@IBDesignable

class NewInputCell : UITableViewCell
{

    
    override func awakeFromNib()
    {
        
    }
    
}
class InputViewController: UIViewController
{
    @IBOutlet var ingredientTextField: UITextField!
    
    var ingredient : [String] = []
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var searchButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchButton.layer.cornerRadius = 15
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.delegate = self
        tableView.dataSource = self
        ingredientTextField.layer.borderWidth = 1
        ingredientTextField.layer.cornerRadius = 17
        ingredientTextField.layer.borderColor =   UIColor.white.cgColor
        searchButton.isEnabled = false

       
    }
    
    @IBAction func pressedEnter(_ sender: UITextField)
    {
        let alert = UIAlertController(title: "Ingredient Missing", message: "Add ingredient or hit search", preferredStyle: .alert)
              let action = UIAlertAction(title: "Continue", style: .default)
              
              if ingredientTextField.text == ""
              {
                  alert.addAction(action)
                  present(alert,animated: true)


              }else{
                  guard let ingredient = ingredientTextField.text else {return}
                  self.add(ingredient)
              }
        textFieldShouldReturn(textField: ingredientTextField)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        ingredientTextField.resignFirstResponder()
       return true
    }
    @IBAction func buttonPushed(_ sender: Any)
    {
        performSegue(withIdentifier: "toResults", sender: nil)
        print(ingredient)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let destination = segue.destination as? RecipeViewController,
                   segue.identifier == UIStoryboardSegue.name.toRestultsOfSearch
               {
                destination.searchInput = ingredient
            
               }
        
    }
   
    
    func add(_ ingredients : String)
    {
        
        ingredient.append(ingredientTextField.text!)
        let indexPath = IndexPath(row: ingredient.count - 1, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .left)
        tableView.endUpdates()
        ingredientTextField.text = ""
        view.endEditing(true)
        
        
    }
}
class GradientView: UIView
{
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map{$0.cgColor}
    }
    
}
extension InputViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if ingredient.count == 0
        {
            searchButton.isEnabled = false
        }
        else
        {
            searchButton.isEnabled = true
        }
         return ingredient.count
    }
    // let itemTitle =
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let ingredientName = ingredient[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? NewInputCell else {fatalError()}
        cell.textLabel?.text = ingredientName
        cell.textLabel?.layer.borderWidth = 0.5
        cell.textLabel?.layer.borderColor = UIColor.white.cgColor
        cell.textLabel?.textAlignment = .center
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if  editingStyle == .delete
        {
            ingredient.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            //tableView.reloadData()
        }
    }
    
    
}

