//
//  RecipeSearchModel.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 03/12/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let root = try? newJSONDecoder().decode(Root.self, from: jsonData)

import Foundation
import UIKit

// MARK: - Root
struct Root: Codable {
    let q: String
    let from: Int
    let to: Int
    let more: Bool
    let count: Int
    let hits: [Hit]

    enum CodingKeys: String, CodingKey {
        case q = "q"
        case from = "from"
        case to = "to"
        case more = "more"
        case count = "count"
        case hits = "hits"
    }
}

// MARK: - Hit
struct Hit: Codable {
    let recipe: Recipe
    let bookmarked: Bool
    let bought: Bool

    enum CodingKeys: String, CodingKey {
        case recipe = "recipe"
        case bookmarked = "bookmarked"
        case bought = "bought"
    }
}

// MARK: - Recipe
struct Recipe: Codable {
    let uri: String
    let label: String
    let image: String
    let source: String
    let url: String
    let shareAs: String
    let yield: Int
    let dietLabels: [String]
    let healthLabels: [String]
    let cautions: [String]
    let ingredientLines: [String]
    let ingredients: [Ingredient]
    let calories: Double
    let totalWeight: Double
    let totalTime: Int
    let totalNutrients: [String: Total]
    let totalDaily: [String: Total]
    let digest: [Digest]

    enum CodingKeys: String, CodingKey {
        case uri = "uri"
        case label = "label"
        case image = "image"
        case source = "source"
        case url = "url"
        case shareAs = "shareAs"
        case yield = "yield"
        case dietLabels = "dietLabels"
        case healthLabels = "healthLabels"
        case cautions = "cautions"
        case ingredientLines = "ingredientLines"
        case ingredients = "ingredients"
        case calories = "calories"
        case totalWeight = "totalWeight"
        case totalTime = "totalTime"
        case totalNutrients = "totalNutrients"
        case totalDaily = "totalDaily"
        case digest = "digest"
    }
}

// MARK: - Digest
struct Digest: Codable {
    let label: String
    let tag: String
    let schemaOrgTag: String?
    let total: Double
    let hasRDI: Bool
    let daily: Double
    let unit: Unit
    let sub: [Digest]?

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case tag = "tag"
        case schemaOrgTag = "schemaOrgTag"
        case total = "total"
        case hasRDI = "hasRDI"
        case daily = "daily"
        case unit = "unit"
        case sub = "sub"
    }
}

enum Unit: String, Codable {
    case empty = "%"
    case g = "g"
    case iu = "IU"
    case kcal = "kcal"
    case mg = "mg"
    case µg = "µg"
}

// MARK: - Ingredient
struct Ingredient: Codable {
    let text: String
    let weight: Double

    enum CodingKeys: String, CodingKey {
        case text = "text"
        case weight = "weight"
    }
}

// MARK: - Total
struct Total: Codable {
    let label: String
    let quantity: Double
    let unit: Unit

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case quantity = "quantity"
        case unit = "unit"
    }
}

struct RecipesOutput
{
    var recipe : Root?
}

class RecipeSearchModel
{
    init()
    {
        
    }
}

