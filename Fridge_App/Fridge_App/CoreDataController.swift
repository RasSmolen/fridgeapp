//
//  CoreDataController.swift
//  Fridge_App
//
//  Created by Rastislav Smolen on 04/12/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//
import Foundation
import CoreData



class CoreDataController
{

    private init(){}
    
    static let shared = CoreDataController()
    
    var mainContext:NSManagedObjectContext
    {
        return persistentContainer.viewContext
    }
    
   var privateContext: NSManagedObjectContext {
       let newConetxt = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
       newConetxt.parent = mainContext
       return newConetxt
   }
   

    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Fridge_App")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return false
    }

}

